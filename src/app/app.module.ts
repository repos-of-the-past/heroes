import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { HeroesComponent } from './pages/heroes/heroes.component';
import { HeroListTopComponent } from './components/hero-list-top/hero-list-top.component';
import { HeroListComponent } from './components/hero-list/hero-list.component';
import { HeroListDetailsComponent } from './components/hero-list-details/hero-list-details.component';
import { HeroPictureComponent } from './components/hero-picture/hero-picture.component';
import { AdminComponent } from './pages/admin/admin.component';
import { AdminHeroListComponent } from './components/admin-hero-list/admin-hero-list.component';
import { AdminHeroEditComponent } from './components/admin-hero-edit/admin-hero-edit.component';
import { AdminHeroAddComponent } from './pages/admin-hero-add/admin-hero-add.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    HeroesComponent,
    HeroListTopComponent,
    HeroListComponent,
    HeroListDetailsComponent,
    HeroPictureComponent,
    AdminComponent,
    AdminHeroListComponent,
    AdminHeroEditComponent,
    AdminHeroAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
