import { Component, OnInit } from '@angular/core';

import { Hero } from '../../models/Hero';
import { HeroService } from '../../services/hero.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  activeHero: Hero;

  constructor(private heroService: HeroService) { }

  ngOnInit() {
  }

  actionEventHandler({action, id}: any): void {
    switch (action) {
      case 'edit':
        this.selectHero(id);
        break;
      case 'delete':
        break;
      default:
    }
  }

  selectHero(id: number): void {
    this.heroService.getHero(id)
      .subscribe(hero => this.activeHero = hero);
  }
}
