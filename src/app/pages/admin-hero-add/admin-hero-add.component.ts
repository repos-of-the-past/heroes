import { Component, OnInit } from '@angular/core';

import { Hero } from '../../models/Hero';

@Component({
  selector: 'app-admin-hero-add',
  templateUrl: './admin-hero-add.component.html',
  styleUrls: ['./admin-hero-add.component.scss']
})
export class AdminHeroAddComponent implements OnInit {
  hero: Hero = {
    id: 1,
    name: "",
    description: "",
    isActive: true
  };

  constructor() { }

  ngOnInit() {
  }

  addHero(hero: Hero): void {
    console.log(hero);
  }
}
