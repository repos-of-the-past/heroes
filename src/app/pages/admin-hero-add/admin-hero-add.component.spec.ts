import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHeroAddComponent } from './admin-hero-add.component';

describe('AdminHeroAddComponent', () => {
  let component: AdminHeroAddComponent;
  let fixture: ComponentFixture<AdminHeroAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHeroAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHeroAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
