import { Component, OnInit } from '@angular/core';

import { Hero } from '../../models/Hero';
import { HeroService } from '../../services/hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {
  activeHero: Hero;

  constructor(private heroService: HeroService) { }

  ngOnInit() {
  }

  idEventHandler($event: number): void {
    this.selectHero($event);
  }

  selectHero(id: number): void {
    this.heroService.getHero(id)
      .subscribe(hero => this.activeHero = hero);
  }
}
