import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Hero } from '../models/Hero';

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  private heroesUrl = '/api/heroes'

  constructor() { }

  getHeroes(): Observable<Hero[]> {
    const heroes: Hero[] = [
      {
        id: 1,
        name: 'Hero 1',
        description: 'Description for Hero 1',
        isActive: true
      },
      {
        id: 2,
        name: 'Hero 2',
        description: 'Description for Hero 2',
        isActive: true
      },
      {
        id: 3,
        name: 'Hero 3',
        description: 'Description for Hero 3',
        isActive: true
      }
    ];

    return of(heroes);
  }

  getHero(id: number): Observable<Hero> {
    const hero: Hero[] = [
      {
        id: 1,
        name: 'Hero 1',
        description: 'Description for Hero 1',
        isActive: true
      },
      {
        id: 2,
        name: 'Hero 2',
        description: 'Description for Hero 2',
        isActive: true
      },
      {
        id: 3,
        name: 'Hero 3',
        description: 'Description for Hero 3',
        isActive: true
      }
    ];

    return of(hero[--id]);
  }

  updateHero(id: number, hero: Hero): Observable<boolean> {
    return of(true);
  }
}
