import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero-picture',
  templateUrl: './hero-picture.component.html',
  styleUrls: ['./hero-picture.component.scss']
})
export class HeroPictureComponent implements OnInit {
  @Input() hero: any;

  constructor() { }

  ngOnInit() {
  }

}
