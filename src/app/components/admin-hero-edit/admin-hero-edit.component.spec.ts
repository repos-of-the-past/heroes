import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHeroEditComponent } from './admin-hero-edit.component';

describe('AdminHeroEditComponent', () => {
  let component: AdminHeroEditComponent;
  let fixture: ComponentFixture<AdminHeroEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHeroEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHeroEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
