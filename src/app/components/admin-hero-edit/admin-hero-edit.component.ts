import { Component, Input, OnInit } from '@angular/core';

import { Hero } from '../../models/Hero';
import { HeroService } from '../../services/hero.service';

@Component({
  selector: 'app-admin-hero-edit',
  templateUrl: './admin-hero-edit.component.html',
  styleUrls: ['./admin-hero-edit.component.scss']
})
export class AdminHeroEditComponent implements OnInit {
  @Input() activeHero: Hero;

  constructor(private heroService: HeroService) { }

  ngOnInit() {
  }

  updateHero(id: number, hero: Hero): void {
    this.heroService.updateHero(id, hero)
      .subscribe(response => console.log(response));
  }
}
