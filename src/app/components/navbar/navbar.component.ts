import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  @Input() isAdmin: boolean;
  links = [];

  constructor() { }

  ngOnInit() {
    this.links = [
      {
        title: 'Home',
        link: '/',
      },
      {
        title: 'Heroes',
        link: '/heroes'
      },
      {
        title: 'Admin',
        link: '/admin'
      }
    ];
  }

  navClasses(): object {
    let classes = {
      'navbar': true,
      'navbar-expand-lg': true,
      'navbar-dark': true,
      'navbar-bg': !this.isAdmin,
      'navbar-bg-admin': this.isAdmin
    };

    return classes;
  }
}
