import { Component, Input, OnInit } from '@angular/core';

import { Hero } from '../../models/Hero';

@Component({
  selector: 'app-hero-list-details',
  templateUrl: './hero-list-details.component.html',
  styleUrls: ['./hero-list-details.component.scss']
})
export class HeroListDetailsComponent implements OnInit {
  @Input() activeHero: Hero;

  constructor() { }

  ngOnInit() {
    console.log(this.activeHero);
  }

}
