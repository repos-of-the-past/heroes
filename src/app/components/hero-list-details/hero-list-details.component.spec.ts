import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroListDetailsComponent } from './hero-list-details.component';

describe('HeroListDetailsComponent', () => {
  let component: HeroListDetailsComponent;
  let fixture: ComponentFixture<HeroListDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroListDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroListDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
