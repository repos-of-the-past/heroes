import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { Hero } from '../../models/Hero';
import { HeroService } from '../../services/hero.service';

@Component({
  selector: 'app-admin-hero-list',
  templateUrl: './admin-hero-list.component.html',
  styleUrls: ['./admin-hero-list.component.scss']
})
export class AdminHeroListComponent implements OnInit {
  @Output() actionEvent = new EventEmitter<object>();

  heroes: Hero[];

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes);
  }

  modifyHero(action: string, id: number): void {
    this.actionEvent.emit({action, id});
  }
}
