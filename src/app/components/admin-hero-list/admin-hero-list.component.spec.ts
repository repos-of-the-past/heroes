import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminHeroListComponent } from './admin-hero-list.component';

describe('AdminHeroListComponent', () => {
  let component: AdminHeroListComponent;
  let fixture: ComponentFixture<AdminHeroListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminHeroListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminHeroListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
