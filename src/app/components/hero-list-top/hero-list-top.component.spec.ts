import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroListTopComponent } from './hero-list-top.component';

describe('HeroListTopComponent', () => {
  let component: HeroListTopComponent;
  let fixture: ComponentFixture<HeroListTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroListTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroListTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
