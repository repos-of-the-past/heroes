import { Component, OnInit } from '@angular/core';

import { Hero } from '../../models/Hero';
import { HeroService } from '../../services/hero.service';

@Component({
  selector: 'app-hero-list-top',
  templateUrl: './hero-list-top.component.html',
  styleUrls: ['./hero-list-top.component.scss']
})
export class HeroListTopComponent implements OnInit {
  heroes: Hero[];

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getTopHeroes();
  }

  getTopHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(hero => this.heroes = hero);
  }
}
