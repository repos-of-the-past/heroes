export class HeroTask {
    id: number;

    task: string;

    isCompleted: boolean;

    isSuccessful: boolean;

    isDeleted: boolean;

    createdAt: string;

    updatedAt: string;

    hero: object;
}
